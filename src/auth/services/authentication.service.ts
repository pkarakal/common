import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { AngularDataContext } from '@themost/angular';
import { ConfigurationService } from '../../shared/services/configuration.service';
import {ActivatedRoute} from '@angular/router';
import { AES, enc, lib, SHA256 } from 'crypto-js';
import { ActivatedUser } from './activated-user.service';
import { ResponseError } from '@themost/client';
import {ApiError, UserProfileNotFoundError} from '../../error/error.custom';
export interface AuthCallbackResponse {
  access_token: string;
  expires_in: number;
  refresh_token?: string;
  refresh_expires_in?: number;
  token_type: string;
  scope: string;
}

export interface OnRefreshToken {
  refresh(): Promise<AuthCallbackResponse | void>;
}

function random(length) {
    let result           = '';
    const characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

@Injectable()
export class AuthenticationService {

    constructor(protected configuration: ConfigurationService,
                protected context: AngularDataContext,
                protected activatedRoute: ActivatedRoute,
                protected activatedUser: ActivatedUser) {
    }

  /**
   * Generates an OAuth2 state param based on the given string.
   * @param {string} value
   * @returns {string}
   */
   protected generateState(value: string): string {
    // get current code_verifier
    const code_verifier = this.getCodeVerifier();
    // encrypt value by using code_verifier
    // and get base64 string
    const b64 = AES.encrypt(value, code_verifier).toString();
    // return state param in hex format
    return enc.Base64.parse(b64).toString(enc.Hex);
  }

  /**
   * Gets an OAuth2 code_verifier
   * @returns {string}
   */
  protected getCodeVerifier(): string {
    let code_verifier = sessionStorage.getItem('code_verifier');
    if (code_verifier == null) {
      // generate
      code_verifier = lib.WordArray.random(24).toString();
      // and store value
      sessionStorage.setItem('code_verifier', code_verifier);
    }
    // finally, return code verifier
    return code_verifier;
  }

    /**
     * Redirects user to OAuth2 server authorization URL. This operation initializes an implicit authorization flow.
     */
  authorize() {
    const settings = this.configuration.settings.auth;
    /* tslint:disable-next-line max-line-length */
        // get scopes
        settings.oauth2.scope = settings.oauth2.scope || [];
        const queryParams = this.activatedRoute.snapshot.queryParams;
        // get continue param or use /
        let continueParam = '/';
        if (queryParams.continue && /^\//.test(queryParams.continue)) {
          continueParam = queryParams.continue;
        }
        const state = this.generateState(continueParam);
        window.location.href = `${settings.authorizeURL}?redirect_uri=${encodeURIComponent(settings.oauth2.callbackURL)}` +
            `&response_type=token&client_id=${settings.oauth2.clientID}` +
            `&scope=${settings.oauth2.scope.join(',')}&state=${state}`;
  }


  protected preCallback(queryParams: any): void {
    if (queryParams.error) {
      console.error('An error occurred on authorization callback');
      console.error(queryParams);
      if (queryParams.error === 'unauthorized_client') {
        throw new ResponseError('Unauthorized client', 499.1);
      }
      throw new ResponseError('Callback error', 499.2);
    }
  }

  /**
   * Handles OAuth2 server callback of an implicit authorization flow.
   * @param {*} queryParams
   * @returns {Promise<*>} - Returns an object which represents the authorized user.
   */
  async callback(queryParams: any): Promise<any> {
      // handle callback errors if any
      this.preCallback(queryParams);
      // call users/me
      this.context.setBearerAuthorization(queryParams.access_token);
      // get user

    try {
      const user = await this.context.model('users/me').asQueryable().expand('groups').getItem();
      if (user == null) {
        throw new Error('Unauthorized');
      }
      // assign token to user
      user.token = Object.assign({
        code_verifier: this.getCodeVerifier()
      }, queryParams, {
          created_at: new Date()
      });
      // clear session storage on success
      sessionStorage.clear();
      // store user to storage
      sessionStorage.setItem('currentUser', JSON.stringify(user));
      // call subscription for ActivatedUser service;
      this.activatedUser.user.next(user);
      // and finally return user
      return user;
    } catch (err) {
      if(err && err.status === 498) {
        throw new UserProfileNotFoundError();
      }
      throw new ApiError( 'Couldn\'t complete authorization callback',401)
    }
  }

    /**
     * Performs user logout.
     * @returns {Promise<boolean>} - Returns true if user has been logged out succesfully. Otherwise returns false.
     */
  async logout(): Promise<boolean> {
    if (typeof sessionStorage.getItem('currentUser') === 'string') {
      // get current user
      const currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
      // get authentication settings
      const settings  = this.configuration.settings.auth;
      // clear session storage
      sessionStorage.clear();
      // redirect to logout uri
      window.location.href = settings.logoutURL;
      return true;
    }
    return false;
  }
}
