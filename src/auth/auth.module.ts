import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {AuthenticationService} from './services/authentication.service';
import {UserService} from './services/user.service';
import {LoginComponent} from './components/login/login.component';
import {LogoutComponent} from './components/logout/logout.component';
import {APP_LOCATIONS, AuthGuard} from './guards/auth.guard';
import {AuthRoutingModule} from './auth.routing';
import {AuthCallbackComponent} from './auth-callback.component';
import {TranslateModule} from '@ngx-translate/core';
import {AngularDataContext, MostModule} from '@themost/angular';
import { ActivatedUser } from './services/activated-user.service';
import { PkceAuthenticationService } from './services/pkce-authentication.service';
import { ConfigurationService } from '../shared/services/configuration.service';
import { ActivatedRoute } from '@angular/router';
import { RefreshTokenComponent } from './components/refresh/refresh-token.component';

export function AuthenticationServiceFactory(
    http: HttpClient,
    configuration: ConfigurationService,
    context: AngularDataContext,
    activatedRoute: ActivatedRoute,
    activatedUser: ActivatedUser) {
    const authSettings: { use?: string } = configuration.settings.auth;
    if (Object.prototype.hasOwnProperty.call(authSettings, 'use')) {
        if (authSettings.use === 'PkceAuthenticationService') {
            // use PkceAuthenticationService
            return new PkceAuthenticationService(http, configuration,
                context, activatedRoute, activatedUser);
        }
        // show warning for unsupported service
        console.log('WARN', 'The specified authentication service is not yet implemented.');
    }
    // use fallback which is of course an instance of SignerService
    return new AuthenticationService(configuration,
        context, activatedRoute, activatedUser);
  }

@NgModule({
    imports: [
        HttpClientModule,
        CommonModule,
        FormsModule,
        TranslateModule,
        MostModule,
        AuthRoutingModule
    ],
    providers: [
        AuthGuard,
        {
            provide: AuthenticationService,
            useFactory: AuthenticationServiceFactory,
            deps: [
                HttpClient,
                ConfigurationService,
                AngularDataContext,
                ActivatedRoute,
                ActivatedUser,
              ],
        },
        UserService,
        ActivatedUser,
        {
            provide: APP_LOCATIONS, useValue: [{
                privilege: 'Location',
                target: {
                    url: '^/auth/'
                },
                mask: 1
            },
                {
                    privilege: 'Location',
                    target: {
                        url: '^/error'
                    },
                    mask: 1
                }]
        }
    ],
    declarations: [
        LoginComponent,
        LogoutComponent,
        AuthCallbackComponent,
        RefreshTokenComponent
    ],
    exports: [
        RefreshTokenComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AuthModule {
}
