import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ErrorBaseComponent, HttpErrorComponent} from './components/error-base/error-base.component';

const routes: Routes = [
  {
    path: 'error',
    component: ErrorBaseComponent
  },
  {
    path: 'error/:status',
    component: HttpErrorComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ErrorRoutingModule {
}
