import { Component, OnInit } from '@angular/core';

/**
 *
 * A native spinner component
 * @export
 * @class SpinnerComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'universis-spinner',
  styleUrls: [`./spinner.component.scss`],
  template: `
      <div class="s--spinner">
        <div class="sk-three-bounce">
          <div class="sk-child sk-bounce1"></div>
          <div class="sk-child sk-bounce2"></div>
          <div class="sk-child sk-bounce3"></div>
        </div>
      </div>
  `
})
export class SpinnerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    //
  }
}
