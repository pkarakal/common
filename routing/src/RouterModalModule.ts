import {NgModule} from '@angular/core';
import { RouterModalComponent } from './RouterModalComponent';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  declarations: [RouterModalComponent],
  imports: [
    RouterModule,
    CommonModule,
    TranslateModule
  ],
  providers: [
  ],
  exports: [RouterModalComponent]
})
export class RouterModalModule {
  constructor() {
    //
  }
}
